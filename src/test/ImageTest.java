package test;

import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertNotNull;

/**
 * @Description:
 * @Author: Fan
 * @Date: 2021/3/28 0:02
 * @Version 1.0
 **/
public class ImageTest {
    @Test
     public void test() {

        try {
            BufferedImage image = ImageIO.read(new File("D:\\Do\\spring\\tank\\src\\images\\bulletD.gif"));
            assertNotNull(image);

            BufferedImage image2 = ImageIO.read(ImageTest.class.getClassLoader().getResourceAsStream("images/bulletD.gif"));
            //this.getClass()
            assertNotNull(image2);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
