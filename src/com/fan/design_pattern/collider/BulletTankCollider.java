package com.fan.design_pattern.collider;

import com.fan.tank.Bullet;
import com.fan.tank.Explode;
import com.fan.tank.GameObject.GameObject;
import com.fan.tank.Tank;

/**
 * 子弹与坦克相撞
 * @Description:
 * @Author: Fan
 * @Date: 2021/4/18 17:35
 * @Version 1.0
 **/
public class BulletTankCollider implements Collider{

    @Override
    public boolean collide(GameObject o1, GameObject o2) {
        if((o1 instanceof Bullet) && (o2 instanceof Tank) ) {
            Bullet bullet = (Bullet) o1;
            Tank tank = (Tank) o2;
            if (bullet.getTankGroup() == tank.getTankGroup()) {
                return true;
            }
            if (bullet.rectangle.intersects(tank.rectangle)) {
                tank.die();
                bullet.die();
                int eX = tank.getX() + Tank.WIDTH/2 - Explode.WIDTH/2;
                int eY = tank.getY() + Tank.HEIGHT/2 - Explode.HEIGHT/2;
                new Explode(eX, eY);
                return false;
            }
//            if(bullet.collideWith(tank)) {
//                return false;
//            }
        }else if (o1 instanceof Tank && o2 instanceof Bullet) {
            return collide(o2, o1);
        }
        return true;
    }
}
