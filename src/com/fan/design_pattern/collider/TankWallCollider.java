package com.fan.design_pattern.collider;

import com.fan.tank.GameObject.GameObject;
import com.fan.tank.Tank;
import com.fan.tank.Wall;

/**
 * 坦克与墙相撞
 * @Description:
 * @Author: Fan
 * @Date: 2021/09/19 16:54
 * @Version 1.0
 **/
public class TankWallCollider implements Collider{

    @Override
    public boolean collide(GameObject o1, GameObject o2) {
        if((o1 instanceof Tank) && (o2 instanceof Wall) ) {
            Tank tank = (Tank) o1;
            Wall wall = (Wall) o2;
            if(tank.rectangle.intersects(wall.rect)) {
                tank.back();
            }
        }else if (o1 instanceof Wall && o2 instanceof Tank) {
            return collide(o2, o1);
        }
        return true;
    }
}
