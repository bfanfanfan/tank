package com.fan.design_pattern.collider;

import com.fan.tank.Bullet;
import com.fan.tank.GameObject.GameObject;
import com.fan.tank.Wall;

/**
 * 子弹与墙相撞
 * @Description:
 * @Author: Fan
 * @Date: 2021/09/19 16:54
 * @Version 1.0
 **/
public class BulletWallCollider implements Collider{

    @Override
    public boolean collide(GameObject o1, GameObject o2) {
        if((o1 instanceof Bullet) && (o2 instanceof Wall) ) {
            Bullet bullet = (Bullet) o1;
            Wall wall = (Wall) o2;
            if(bullet.rectangle.intersects(wall.rect)) {
                bullet.die();
            }
        }else if (o1 instanceof Wall && o2 instanceof Bullet) {
            return collide(o2, o1);
        }
        return true;
    }
}
