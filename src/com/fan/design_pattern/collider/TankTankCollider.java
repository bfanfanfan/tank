package com.fan.design_pattern.collider;

import com.fan.tank.GameObject.GameObject;
import com.fan.tank.Tank;

/**
 * 坦克之间相撞
 * @Description:
 * @Author: Fan
 * @Date: 2021/4/18 17:35
 * @Version 1.0
 **/
public class TankTankCollider implements Collider{

    @Override
    public boolean collide(GameObject o1, GameObject o2) {
        if((o1 instanceof Tank) && (o2 instanceof Tank) ) {
            Tank tank1 = (Tank) o1;
            Tank tank2 = (Tank) o2;
            if (tank1.getRectangle().intersects(tank2.getRectangle())) {
                // 坦克相撞后各自回到上一步的位置
                tank1.back();
                tank2.back();

                // 相撞后停止--不合适
//                tank1.stop();
//                tank2.stop();
            }
        }
        return true;
    }
}
