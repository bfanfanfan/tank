package com.fan.design_pattern.collider;

import com.fan.tank.GameObject.GameObject;

import java.util.LinkedList;
import java.util.List;

/**
 * 采用责任链模式，将各种碰撞情况封装为一个碰撞链
 * @Description:
 * @Author: Fan
 * @Date: 2021/4/18 22:03
 * @Version 1.0
 **/
public class ColliderChain implements Collider{
    private List<Collider> colliders = new LinkedList<>();

    public ColliderChain() {
        add(new BulletTankCollider());
        add(new TankTankCollider());
        add(new BulletWallCollider());
        add(new TankWallCollider());
    }

    public void add(Collider c) {
        colliders.add(c);
    }

    @Override
    public boolean collide(GameObject o1, GameObject o2) {
        for (int i = 0; i < colliders.size(); i++) {
            if(!colliders.get(i).collide(o1, o2)) {
                return false;
            }
        }
        return true;
    }
}
