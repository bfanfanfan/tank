package com.fan.design_pattern.collider;

import com.fan.tank.GameObject.GameObject;

/**
 * 碰撞器
 * @Description:
 * @Author: Fan
 * @Date: 2021/4/18 17:32
 * @Version 1.0
 **/
public interface Collider {
    /**
     * 是否在责任链中往下传递
     * @param o1
     * @param o2
     * @return
     */
    boolean collide(GameObject o1, GameObject o2);
}
