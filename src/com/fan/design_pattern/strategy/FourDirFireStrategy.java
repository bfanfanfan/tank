package com.fan.design_pattern.strategy;

import com.fan.common.Audio;
import com.fan.tank.Bullet;
import com.fan.tank.Tank;
import com.fan.util.Dir;
import com.fan.util.TankGroup;

/**
 * 设置我方坦克开火策略：一次开火四个方向均发射子弹
 * @Author: fan
 * @DateTime: 2021-04-14 17:52
 **/
public class FourDirFireStrategy implements FireStrategy {

    @Override
    public void fire(Tank t) {
        int bulletX = t.x + Tank.WIDTH / 2 - Bullet.WIDTH / 2;
        int bulletY = t.y + Tank.HEIGHT / 2 - Bullet.HEIGHT / 2;
        Dir[] dirs = Dir.values();
        for (Dir dir : dirs) {
            new Bullet(bulletX, bulletY, dir, t.tankGroup);
            //工厂模式
//            t.tankFrame.gameFactory.createBullet(bulletX, bulletY, dir, t.tankGroup, t.tankFrame);
        }

        if (t.tankGroup == TankGroup.GoodTank) {
            new Thread(() -> new Audio("audio/tank_fire.wav").play()).start();
        }
    }
}
