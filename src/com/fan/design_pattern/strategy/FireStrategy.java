package com.fan.design_pattern.strategy;

import com.fan.tank.Tank;

/**
 * 开火策略
 * @Author: fan
 * @DateTime: 2021-04-14 17:24
 **/
public interface FireStrategy {
    void fire(Tank t);
}

