package com.fan.design_pattern.strategy;

import com.fan.common.Audio;
import com.fan.tank.Bullet;
import com.fan.tank.Tank;
import com.fan.util.TankGroup;

/**
 * 默认开火策略
 * @Author: fan
 * @DateTime: 2021-04-14 17:26
 **/
public class DefaultFireStrategy implements FireStrategy{

    @Override
    public void fire(Tank t) {
        int bulletX = t.x + Tank.WIDTH/2 - Bullet.WIDTH/2;
        int bulletY = t.y + Tank.HEIGHT/2 - Bullet.HEIGHT/2;
        new Bullet(bulletX,bulletY,t.dir,t.tankGroup);
        if(t.tankGroup == TankGroup.GoodTank) {
            new Thread(() -> new Audio("audio/tank_fire.wav").play()).start();
        }
    }
}
