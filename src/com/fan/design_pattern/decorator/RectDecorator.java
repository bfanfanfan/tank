package com.fan.design_pattern.decorator;

import com.fan.tank.GameObject.GameObject;

import java.awt.*;

/**
 * @Description: 加方框
 * @Author: Fan
 * @Date: 2021/9/20 9:20
 * @Version 1.0
 **/
public class RectDecorator extends GODecorator{

    public RectDecorator(GameObject go) {
        super(go);
    }

    @Override
    public void paint(Graphics g) {
        // 装饰器的位置要随着被装饰的对象的位置变化
        this.x = go.x;
        this.y = go.y;

        go.paint(g);

        Color c = g.getColor();
        g.setColor(Color.YELLOW);
        g.drawRect(go.x, go.y, go.getWidth() + 2, go.getHeight() + 2);
        g.setColor(c);
    }

    @Override
    public int getWidth() {
        return super.go.getWidth();
    }

    @Override
    public int getHeight() {
        return super.go.getHeight();
    }
}
