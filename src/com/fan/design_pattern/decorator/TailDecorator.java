package com.fan.design_pattern.decorator;

import com.fan.tank.GameObject.GameObject;

import java.awt.*;

/**
 * @Description: 加尾巴
 * @Author: Fan
 * @Date: 2021/9/20 9:20
 * @Version 1.0
 **/
public class TailDecorator extends GODecorator{

    public TailDecorator(GameObject go) {
        super(go);
    }

    @Override
    public void paint(Graphics g) {
        this.x = go.x;
        this.y = go.y;

        go.paint(g);

        Color c = g.getColor();
        g.setColor(Color.YELLOW);
        g.drawLine(go.x, go.y, go.x + getWidth(), y + getHeight());
        g.setColor(c);
    }

    @Override
    public int getWidth() {
        return super.go.getWidth();
    }

    @Override
    public int getHeight() {
        return super.go.getHeight();
    }
}
