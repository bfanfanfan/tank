package com.fan.design_pattern.decorator;

import com.fan.tank.GameObject.GameObject;

import java.awt.*;

/**
 * @Description:
 * @Author: Fan
 * @Date: 2021/9/20 9:18
 * @Version 1.0
 **/
public abstract class GODecorator extends GameObject {

    public GameObject go;

    public GODecorator(GameObject go) {
        this.go = go;
    }

    @Override
    public abstract void paint(Graphics g);

}
