package com.fan.design_pattern.observer;

import com.fan.tank.Tank;

/**
 * @Description: 触发事件的事件源-tank
 * @Author: Fan
 * @Date: 2021/9/21 10:03
 * @Version 1.0
 **/
public class TankFireEvent {
    Tank tank;

    public Tank getSource() {
        return tank;
    }

    public TankFireEvent(Tank tank) {
        this.tank = tank;
    }
}
