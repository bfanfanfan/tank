package com.fan.design_pattern.observer;

import com.fan.tank.Tank;

/**
 * @Description: 观察者处理事件
 * @Author: Fan
 * @Date: 2021/9/21 10:07
 * @Version 1.0
 **/
public class TankFireHandler implements TankFireObserver{
    @Override
    public void actionOnFire(TankFireEvent e) {
        Tank t = e.getSource();
        t.fire();
    }
}
