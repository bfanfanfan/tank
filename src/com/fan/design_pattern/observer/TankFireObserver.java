package com.fan.design_pattern.observer;


public interface TankFireObserver {
    public void actionOnFire(TankFireEvent e);
}
