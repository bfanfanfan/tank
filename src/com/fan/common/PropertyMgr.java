package com.fan.common;

import java.io.IOException;
import java.util.Properties;

/**
 * @Description:
 * @Author: Fan
 * @Date: 2021/3/28 21:49
 * @Version 1.0
 **/
public class PropertyMgr {

    public static Properties props = new Properties();

    static {
        try {
            props.load(PropertyMgr.class.getClassLoader().getResourceAsStream("com/fan/config/config"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Object get(String key) {
        if(props == null) {
            return null;
        }
        return props.get(key);
    }

    public static int getInt(String key) {
        return (int)props.get(key);
    }

    public static String getString(String key) {
        return (String)props.get(key);
    }

    /**
     * 测试
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(PropertyMgr.get("gameHeight"));
    }
}
