package com.fan.frame;

import com.fan.tank.Bullet;
import com.fan.tank.Explode;
import com.fan.tank.GameModel.GameModel;
import com.fan.tank.Tank;
import com.fan.util.Dir;
import com.fan.util.TankGroup;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: fan
 * @DateTime: 2021-03-26 10:22
 **/
public class TankFrame extends Frame {
    GameModel gameModel = GameModel.getInstance();
    public static final int GAME_WIDTH = 1080, GAME_HEIGHT = 960;

    public TankFrame() {
        this.setSize(GAME_WIDTH, GAME_HEIGHT);
        this.setLocation(500, 300);
        //改变大小
        this.setResizable(false);
        this.setTitle("Tank War");
        this.setVisible(true);

        this.addKeyListener(new MyKeyListener());

        // 添加window监听器，并通过匿名类WindowAdapter中的windowClosing方法，监听点击x后关闭窗口
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
    }

    Image offScreenImage = null;

    /**
     * repaint将先调用update，再调用paint
     * 双缓冲解决界面闪烁问题：先在内存画一张图片，使用该图片覆盖原来的界面
     *
     * @param g
     */
    @Override
    public void update(Graphics g) {
        if (offScreenImage == null) {
            offScreenImage = this.createImage(GAME_WIDTH, GAME_HEIGHT);
        }
        Graphics graphics = offScreenImage.getGraphics();
        Color c = graphics.getColor();
        g.setColor(Color.BLACK);
        graphics.fillRect(0, 0, GAME_WIDTH, GAME_HEIGHT);
        graphics.setColor(c);
        paint(graphics);
        g.drawImage(offScreenImage, 0, 0, null);
    }

    /**
     * 每次窗口出现在桌面最顶层的时候调用。Graphics相当于一个画笔
     *
     * @param g
     */
    @Override
    public void paint(Graphics g) {
        gameModel.paint(g);
    }

    /**
     * 键盘监听
     */
    class MyKeyListener extends KeyAdapter {

        boolean bL = false;
        boolean bU = false;
        boolean bR = false;
        boolean bD = false;

        /**
         * 键类型
         *
         * @param e
         */
        @Override
        public void keyTyped(KeyEvent e) {

        }

        /**
         * 键按下触发
         *
         * @param e
         */
        @Override
        public void keyPressed(KeyEvent e) {
            int key = e.getKeyCode();
            switch (key) {
                case KeyEvent.VK_LEFT:
                    bL = true;
                    break;
                case KeyEvent.VK_UP:
                    bU = true;
                    break;
                case KeyEvent.VK_RIGHT:
                    bR = true;
                    break;
                case KeyEvent.VK_DOWN:
                    bD = true;
                    break;
                default:
                    break;
            }
            setMainTankDir();
        }

        /**
         * 键松开触发
         *
         * @param e
         */
        @Override
        public void keyReleased(KeyEvent e) {
            int key = e.getKeyCode();
            switch (key) {
                case KeyEvent.VK_LEFT:
                    bL = false;
                    break;
                case KeyEvent.VK_UP:
                    bU = false;
                    break;
                case KeyEvent.VK_RIGHT:
                    bR = false;
                    break;
                case KeyEvent.VK_DOWN:
                    bD = false;
                    break;
                case KeyEvent.VK_CONTROL:
                    gameModel.getMainTank().fire();
                    // 观察者模式
//                    gameModel.getMainTank().handleFireKey();
                    break;
                default:
                    break;
            }
            setMainTankDir();
        }

        /**
         * 判断坦克方向
         */
        private void setMainTankDir() {
            Tank myTank = gameModel.getMainTank();
            if (!bL && !bU && !bR && !bD) {
                myTank.setMoving(false);
            } else {
                myTank.setMoving(true);
                if (bL) {
                    myTank.setDir(Dir.LEFT);
                }
                if (bU) {
                    myTank.setDir(Dir.UP);
                }
                if (bR) {
                    myTank.setDir(Dir.RIGHT);
                }
                if (bD) {
                    myTank.setDir(Dir.DOWN);
                }
            }
        }
    }
}
