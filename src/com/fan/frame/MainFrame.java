package com.fan.frame;

import com.fan.common.PropertyMgr;
import com.fan.frame.TankFrame;
import com.fan.tank.Tank;
import com.fan.util.Dir;
import com.fan.util.TankGroup;

/**
 * @Author: fan
 * @DateTime: 2021-03-26 10:06
 **/
public class MainFrame {
    public static void main(String[] args) throws InterruptedException {
        TankFrame tankFrame = new TankFrame();

        while (true) {
            Thread.sleep(50);
            // 刷新窗口
            tankFrame.repaint();
        }
    }
}
