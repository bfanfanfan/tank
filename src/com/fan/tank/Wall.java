package com.fan.tank;

import com.fan.tank.GameModel.GameModel;
import com.fan.tank.GameObject.GameObject;

import java.awt.*;

/**
 * @Description: 墙
 * @Author: Fan
 * @Date: 2021/9/19 17:24
 * @Version 1.0
 **/
public class Wall extends GameObject {
    /**
     * 墙宽、高
     */
    int w, h;

    public Rectangle rect;

    public Wall(int x, int y, int w, int h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.rect = new Rectangle(x, y, w, h);

        GameModel.getInstance().add(this);
    }

    @Override
    public void paint(Graphics g) {
        Color c = g.getColor();
        g.setColor(Color.DARK_GRAY);
        g.fillRect(x, y, w, h);
        g.setColor(c);
    }

    @Override
    public int getWidth() {
        return w;
    }

    @Override
    public int getHeight() {
        return h;
    }
}
