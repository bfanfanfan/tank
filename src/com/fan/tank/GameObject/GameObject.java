package com.fan.tank.GameObject;

import java.awt.*;

/**
 * 使用调停者模式，用于统一管理于所有游戏组件
 * @Description:
 * @Author: Fan
 * @Date: 2021/4/18 11:46
 * @Version 1.0
 **/
public abstract class GameObject {
    /**
     * 坐标
     */
    public int x, y;

    /**
     * 绘制
     * @param g
     */
    public abstract void paint(Graphics g);

    public abstract int  getWidth();

    public abstract int  getHeight();

}
