package com.fan.tank.GameModel;

import com.fan.common.PropertyMgr;
import com.fan.design_pattern.observer.TankFireEvent;
import com.fan.design_pattern.observer.TankFireHandler;
import com.fan.design_pattern.observer.TankFireObserver;
import com.fan.tank.GameObject.GameObject;
import com.fan.tank.Tank;
import com.fan.tank.Wall;
import com.fan.design_pattern.collider.ColliderChain;
import com.fan.util.Dir;
import com.fan.util.TankGroup;

import java.awt.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 使用门面模式封装坦克相关
 * @Description:
 * @Author: Fan
 * @Date: 2021/4/18 10:38
 * @Version 1.0
 **/
public class GameModel{

    private static final GameModel INSTANCE = new GameModel();

    static {
        INSTANCE.init();
    }

    Tank tank;
//    public List<Bullet> bullets = new ArrayList<>();
//    public List<Tank> tanks = new ArrayList<>();
//    public List<Explode> explodes = new ArrayList<>();

    List<GameObject> gameObjects = new ArrayList<>();
    ColliderChain chain = new ColliderChain();

    public static GameModel getInstance() {
        return INSTANCE;
    }

    private GameModel() {}

    private void init() {
        // 初始化主战坦克
        tank = new Tank(200, 400, Dir.RIGHT, TankGroup.GoodTank);

        int initTankCount = Integer.parseInt((String) PropertyMgr.get("initTankCount"));
        // 初始化敌方坦克
        for(int i=0; i<initTankCount; i++) {
            new Tank(50 + i*80, 200, Dir.DOWN, TankGroup.BadTank);
        }

        // 初始化墙
        new Wall(150, 150, 200, 50);
        new Wall(550, 150, 200, 50);
        new Wall(300, 300, 50, 200);
        new Wall(550, 300, 50, 200);
    }

    public void add(GameObject go) {
        this.gameObjects.add(go);
    }

    public void remove(GameObject go) {
        this.gameObjects.remove(go);
    }

    /**
     * 坦克绘制
     * @param g
     */
    public void paint(Graphics g) {
        /*Color color = g.getColor();
        g.setColor(Color.WHITE);
        g.drawString("子弹数量：" + bullets.size(), 10, 60);
        g.drawString("敌方数量：" + tanks.size(), 10, 80);
        g.drawString("爆炸数量：" + explodes.size(), 10, 100);
        g.setColor(color);*/

        // 己方坦克绘制
        tank.paint(g);

        for (int i = 0; i < gameObjects.size(); i++) {
            gameObjects.get(i).paint(g);
        }

        // 互相碰撞
        for (int i = 0; i < gameObjects.size(); i++) {
            for (int j = i+1; j < gameObjects.size(); j++) {
                GameObject o1 = gameObjects.get(i);
                GameObject o2 = gameObjects.get(j);
                chain.collide(o1, o2);
            }
        }
    }

    public Tank getMainTank() {
        return tank;
    }

    /**
     * 观察者模式
     */
    // 可以有多个观察者
    /*private List<TankFireObserver> fireObservers = Arrays.asList(new TankFireHandler());
    public void handleFireKey() {
        TankFireEvent event = new TankFireEvent(this.getMainTank());
        for(TankFireObserver observer : fireObservers) {
            observer.actionOnFire(event);
        }
    }*/
}
