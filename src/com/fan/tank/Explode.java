package com.fan.tank;

import com.fan.common.Audio;
import com.fan.common.ResourceMgr;
import com.fan.frame.TankFrame;
import com.fan.tank.GameModel.GameModel;
import com.fan.tank.GameObject.GameObject;
import com.fan.util.Dir;
import com.fan.util.TankGroup;

import java.applet.AudioClip;
import java.awt.*;

/**
 * 爆炸封装
 * @Author: fan
 * @DateTime: 2021-03-26 16:28
 **/
public class Explode extends GameObject {
    public static int WIDTH = ResourceMgr.explodes[0].getWidth();
    public static int HEIGHT = ResourceMgr.explodes[0].getHeight();
//    private int x, y;

    private boolean living = true;
    GameModel gameModel = null;

    /**
     * 计数爆炸画到了第几张图片
     */
    private int step = 0;

    public Explode(int x, int y) {
        this.x = x;
        this.y = y;

        GameModel.getInstance().add(this);

        new Thread(() -> new Audio("audio/explode.wav").play()).start();
    }

    @Override
    public void paint(Graphics g) {
        g.drawImage(ResourceMgr.explodes[step++], x, y, null);
        if(step >= ResourceMgr.explodes.length) {
            GameModel.getInstance().remove(this);
        }
    }

    @Override
    public int getWidth() {
        return WIDTH;
    }

    @Override
    public int getHeight() {
        return HEIGHT;
    }
}
