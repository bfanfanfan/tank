package com.fan.tank;

import com.fan.common.Audio;
import com.fan.common.ResourceMgr;
import com.fan.frame.TankFrame;
import com.fan.tank.GameModel.GameModel;
import com.fan.tank.GameObject.GameObject;
import com.fan.util.Dir;
import com.fan.util.TankGroup;
import jdk.nashorn.internal.ir.CallNode;

import java.awt.*;

/**
 * 子弹封装
 * @Author: fan
 * @DateTime: 2021-03-26 16:28
 **/
public class Bullet extends GameObject {
    // 子弹速度
    private static final int SPEED = 10;
    // 子弹大小
    public static int WIDTH = ResourceMgr.bulletD.getWidth();
    public static int HEIGHT = ResourceMgr.bulletD.getHeight();
    // 子弹位置
//    private int x, y;
    // 子弹方向
    private Dir dir;

    // 判断子弹是否存活
    private boolean living = true;

    private TankGroup tankGroup = TankGroup.BadTank;

    public Rectangle rectangle = new Rectangle();

    public Bullet(int x, int y, Dir dir, TankGroup tankGroup) {
        this.x = x;
        this.y = y;
        this.dir = dir;
        this.tankGroup = tankGroup;

        rectangle.x = this.x;
        rectangle.y = this.y;
        rectangle.width = WIDTH;
        rectangle.height = HEIGHT;

        GameModel.getInstance().add(this);
    }

    /**
     * 子弹绘制
     * @param g
     */
    @Override
    public void paint(Graphics g) {
        if(!living) {
            GameModel.getInstance().remove(this);
        }
        switch (dir) {
            case LEFT:
                g.drawImage(ResourceMgr.bulletL, x, y, null);
                break;
            case UP:
                g.drawImage(ResourceMgr.bulletU, x, y, null);
                break;
            case RIGHT:
                g.drawImage(ResourceMgr.bulletR, x, y, null);
                break;
            case DOWN:
                g.drawImage(ResourceMgr.bulletD, x, y, null);
                break;
            default:
                break;
        }
        move();
    }

    @Override
    public int getWidth() {
        return WIDTH;
    }

    @Override
    public int getHeight() {
        return HEIGHT;
    }

    /**
     * 方向控制
     */
    private void move() {
        switch (dir) {
            case LEFT:
                x -= SPEED;
                break;
            case UP:
                y -= SPEED;
                break;
            case RIGHT:
                x += SPEED;
                break;
            case DOWN:
                y += SPEED;
                break;
            default:
                break;
        }

        rectangle.x = this.x;
        rectangle.y = this.y;

        if (x < 0 || y < 0 || x > TankFrame.GAME_WIDTH || y > TankFrame.GAME_HEIGHT) {
            living = false;
        }
    }

    /**
     * 碰撞检测--移入到责任链中降低耦合
     * @param tank
     */
   /* public boolean collideWith(Tank tank) {
        if(this.tankGroup == tank.getTankGroup()) {
            return false;
        }
        // 判断两个方块是否相交
        if(rectangle.intersects(tank.rectangle)) {
            tank.die();
            this.die();
            int eX = tank.getX() + Tank.WIDTH/2 - Explode.WIDTH/2;
            int eY = tank.getY() + Tank.HEIGHT/2 - Explode.HEIGHT/2;
            gameModel.add(new Explode(eX, eY, gameModel));
            return true;
        }
        return false;
    }*/

    /**
     * 子弹消失
     */
    public void die() {
        this.living = false;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public TankGroup getTankGroup() {
        return tankGroup;
    }

    public void setTankGroup(TankGroup tankGroup) {
        this.tankGroup = tankGroup;
    }
}
