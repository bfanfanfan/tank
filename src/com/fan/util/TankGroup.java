package com.fan.util;

/**
 * 己方坦克与敌方坦克做区分
 */
public enum TankGroup {
    GoodTank, BadTank
}
