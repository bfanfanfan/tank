package com.fan.util;

/**
 * 坦克方向
 * @Author: fan
 * @DateTime: 2021-03-26 15:20
 **/
public enum Dir {
    LEFT, UP, RIGHT, DOWN
}
